public class AQueue<T> {

   private T[] arr;
   private int front;
   private int end;
   private int count;
   
   public static class MyException extends RuntimeException
   {
      public MyException()
      {
         super();
      }
      public MyException(String message)
      {
         super(message);
      }
   
   }
      
   public AQueue(int size) {
      
      arr = (T[]) new Object[size];
      front = 0;
      end = -1;
      count = 0;
   
   }
   
   public boolean isEmpty() {
      
      return count == 0;
      
   }
   
   public void enqueue(T data) {
      
      if(count == arr.length) {
        
         T[] arr_new = (T[]) new Object[count*2];
         
         for(int i=0; i<count; i++) {
            
            arr_new[i] = arr[front];
            front = (front + 1) % arr.length;
         
         }
         
         arr = arr_new;
         front = 0;
         count++;
         end = count - 1;
         arr[end] = data;
         
      }
      else {
        
         end = (end + 1) % arr.length;
         arr[end] = data;
         count++;
         
      }
      
   }
   
   public T dequeue() {
      
      T temp;
      if(count == 0) {
         
         throw new MyException();
         
      }
      else {
         
         temp = arr[front];
         arr[front] = null;
         front = (front + 1) % arr.length;
         count--;
         
      }
      
      return temp;
      
   }
   
   public void printArray() {
      
      for(int i=0; i<arr.length; i++) {
         
         System.out.print(arr[i] + " "); 
      
      }
   
   }
   
}

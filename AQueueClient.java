import java.util.Scanner;


public class AQueueClient {

   public static void main(String[] args)
   {
      int size = 5;
      AQueue<Float> queue = new AQueue<Float>(size);
      Scanner input = new Scanner(System.in);

      System.out.print("Enter values: ");
      
      while(input.hasNext()) {
         if(input.hasNextFloat()) {
            queue.enqueue(input.nextFloat());
         }
         else
            input.next();
      }
      
      while(!queue.isEmpty()) {
         
         System.out.print(queue.dequeue() + " ");
         
      }
      
   }
}

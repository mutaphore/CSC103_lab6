import java.util.Scanner;

public class AQueueDriver {
   
   public static void main(String[] args)
   {
      int size = 5;
      AQueue<Integer> queue = new AQueue<Integer>(size);
      Scanner input = new Scanner(System.in);
      char choice = 'z';
      int temp;
      
      System.out.println("Choose one of the following operations:");
      System.out.println("- enqueue/add (enter the letter a)");
      System.out.println("- dequeue/delete (enter the letter d)");
      System.out.println("- check if the list is empty (enter the letter e)");
      System.out.println("- print queue (enter the letter p");
      System.out.println("- quit (enter the letter q)");
     
      while(choice != 'q')
      {
         System.out.println("Enter choice: ");
         choice = input.nextLine().charAt(0);

         switch (choice)
         {
            case 'a':
               System.out.println("Please enter value to be added to queue: ");
               if(input.hasNextInt())
               {
                  temp = input.nextInt();
                  input.nextLine();
                  queue.enqueue(temp);
                  System.out.println("Added " + temp);
               }
               else
               {
                  System.out.println("Invalid value!");
                  input.nextLine();
               }
               break;
            case 'd':
               try
               {
                  temp = queue.dequeue();
                  System.out.println("Removed " + temp);
               }
               catch (AQueue.MyException e)
               {
                  System.out.println("Invalid operation: queue is empty!");
               }
               break;
            case 'e':
               if(queue.isEmpty())
                  System.out.println("empty");
               else
                  System.out.println("not empty");
               break;
            case 'p':
               queue.printArray();
               System.out.println();
               break;
            case 'q':
                  System.out.println("Quitting");
                  while(!queue.isEmpty()) {
                     
                     System.out.print(queue.dequeue() + " ");
                     
                  }
               break;
            default:
               System.out.println("Invalid choice!");
               break;     
         }
      }
   }
   
}
